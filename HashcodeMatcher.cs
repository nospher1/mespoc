﻿
using System;
using System.Collections.Generic;
using System.Collections.Concurrent;

public class Program
{
    static void Main()
    {

        Console.WriteLine( "AaAaAa is" + "AaAaAa".GetHashCode());
        Console.WriteLine( "AaAaBB is" + "AaAaBB".GetHashCode());
        Console.WriteLine( "AaAaAa is" + "AaAaAa".GetHashCode());
        /* foreach (String pass in testRandomString(3))
         {
             Console.WriteLine(pass + " hashcode is " + pass.GetHashCode());
         }*/
        foreach (String pass in testRandomString8(3, 400000000, 500000, 90000))
         {
             Console.WriteLine(pass + " hashcode is" + pass.GetHashCode());
         }
         /*foreach (String pass in testRandomString(3))
         {
             Console.WriteLine(pass + " hashcode is" + pass.GetHashCode());
         }*/
    }

    private static List<String> testRandomString5(int matchNumber){

        var lookup = new Dictionary<int,List<string>>();
        
        
        List<string> datas = getDatas(4);
        for (int i = 0; i <datas.Count; i++)
        {
       
            var h = datas[i].GetHashCode();
          
            List<string> s2;
            if (lookup.TryGetValue(h, out s2) && !s2.Contains( datas[i]) ) {
                s2.Add( datas[i]);
                if(s2.Count == matchNumber){
                    Console.WriteLine("iteration needed to find " +matchNumber+ " similar hashcode for different word : "+ i);
                    return s2;
                }
                    
            }
            else{
                lookup[h] = new List<String>{ datas[i]};
            }

        }
 
        throw new Exception("Too much processing required - review your algorithm");
        
    }

        private static List<String> testRandomString(int matchNumber){

        var lookup = new Dictionary<int,List<string>>();
        int maxIter = 10000000;
        
        for (int i = 0; i < maxIter; i++)
        {
            var s = RandomString2();        
            var h = s.GetHashCode();
          
            List<string> s2;
            if (lookup.TryGetValue(h, out s2) && !s2.Contains(s) ) {
                s2.Add(s);
                if(s2.Count == matchNumber){
                    Console.WriteLine("iteration needed to find " +matchNumber+ " similar hashcode for different word : "+ i);
                    return s2;
                }
                    
            }
            else{
                lookup[h] = new List<String>{s};
            }

        }
 
        throw new Exception("Too much processing required - review your algorithm");
        
    }

        private static List<String> testRandomString8(int matchNumber, int maxIteration, int maxDataSize, long maxTimer){

        var lookup = new Dictionary<int,List<string>>();
        var duets = new Dictionary<int,List<string>>();
        

        var watch = System.Diagnostics.Stopwatch.StartNew();
        // the code that you want to measure comes here

        
        for (int i = 0; i < maxIteration; i++)
        {
            if(maxTimer < watch.ElapsedMilliseconds)
                throw new Exception("Time's UP !");
            
                
            var s = RandomString2();        
            var h = s.GetHashCode();
          
            List<string> s2;
            if (lookup.TryGetValue(h, out s2) && !s2.Contains(s) ) {
                s2.Add(s);
                duets[h] = s2;
                if(s2.Count == matchNumber){
                    Console.WriteLine("iteration needed to find " +matchNumber+ " similar hashcode for different word : "+ i);
                    return s2;
                }
                    
            }
            else{
                
                if(lookup.Count < maxDataSize)
                    lookup[h] = new List<string>{s};
                else{
                    if(duets.Count < maxDataSize)
                        lookup = new Dictionary<int,List<string>>(duets);
                    else
                        throw new Exception("Duet size overflow - review your algorithm");
                }
                    
                
            }
        }



        throw new Exception("Too much processing required - review your algorithm");
        
        
    }

    private static List<String> testRandomString2(int matchNumber, int maxIteration, int maxDataSize, long maxTimer){

        var lookup = new Dictionary<int,List<string>>();
        var duets = new Dictionary<int,List<string>>();
        

        var watch = System.Diagnostics.Stopwatch.StartNew();
        // the code that you want to measure comes here

        
        for (int i = 0; i < maxIteration; i++)
        {
            if(maxTimer < watch.ElapsedMilliseconds)
                throw new Exception("Time's UP !");
            
                
            var s = RandomString();        
            var h = s.GetHashCode();
          
            List<string> s2;
            if (lookup.TryGetValue(h, out s2) && !s2.Contains(s) ) {
                s2.Add(s);
                duets[h] = s2;
                if(s2.Count == matchNumber){
                    Console.WriteLine("iteration needed to find " +matchNumber+ " similar hashcode for different word : "+ i);
                    return s2;
                }
                    
            }
            else{
                if(lookup.Count < maxDataSize || maxTimer <  watch.ElapsedMilliseconds/2)
                    lookup[h] = new List<String>{s};
                else
                    break;
            }
        }
        lookup = null;

        List<string> retour = null;
        var result = Parallel.For(0 , maxIteration, (i, state) => {
            var s = RandomString2();        
            var h = s.GetHashCode();
          
            List<string> s3;
            if (duets.TryGetValue(h, out s3) && !s3.Contains(s) ) {
                s3.Add(s);
                retour = s3;
                state.Break();
                    
            }
        });

        return retour;
        
    }


    private static List<String> testRandomString3(int matchNumber){

        var lookup = new Dictionary<int,List<string>>();
        int maxIter = 10000000;
        
        for (int i = 0; i < maxIter; i++)
        {
            var s = RandomString();        
            var h = s.GetHashCode();
          
            List<string> s2;
            if (lookup.TryGetValue(h, out s2) && !s2.Contains(s) ) {
                s2.Add(s);
                if(s2.Count == matchNumber){
                    Console.WriteLine("iteration needed to find " +matchNumber+ " similar hashcode for different word : "+ i);
                    return s2;
                }
                    
            }
            else{
                lookup[h] = new List<String>{s};
            }

        }
 
        throw new Exception("Too much processing required - review your algorithm");
        
    }

     static Random r = new Random();

    // Define other methods and classes here
    static string RandomString() {

        var s = ((char)r.Next((int)'A',((int)'z')+1)).ToString() +
                ((char)r.Next((int)'A',((int)'z')+1)).ToString() +
                ((char)r.Next((int)'A',((int)'z')+1)).ToString() ;

        return s;
    }

    static string RandomString2() {

        var s = ((char)r.Next(33,126)).ToString() +
                ((char)r.Next(33,126)).ToString() +
                ((char)r.Next(33,126)).ToString() +
                (((char)r.Next(33,126)).ToString() );

        return s;
    }

    static List<String> getDatas(int size){
        var alphabet = "[]()'.,?-éàabcdeABCDE";
        var q = alphabet.Select(x => x.ToString());
       
        for (int i = 0; i < size - 1; i++)
            q = q.SelectMany(x => alphabet, (x, y) => x + y);

        return q.ToList();

    }

}