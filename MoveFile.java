import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class MoveFile {

	private String cheminFichier1 = null;
	private String cheminFichier2 = null;
	private FileSystem fileSystem = FileSystems.getDefault();
	
	public static void main(String[] args) {
		
		try {
			new MoveFile(args[0], args[1]);
		} catch (Exception e) {
			System.out.println("Exception non g�r�e : " + e.getMessage());
		}

	}
	
	public MoveFile(String chemin1, String chemin2){
		
		cheminFichier1 = chemin1 + (chemin1.endsWith(fileSystem.getSeparator()) ? "" : fileSystem.getSeparator()) + "test.txt";
		cheminFichier2 = chemin2 + (chemin2.endsWith(fileSystem.getSeparator()) ? "" : fileSystem.getSeparator()) + "test2.txt";
				
		System.out.println("Chemin1 = "+ cheminFichier1);
		System.out.println("Chemin2 = "+ cheminFichier2);
		
		Init();
		
	}
	
	private void Init()
	{
		
		deplacer1();
		deplacer2();
		deplacer3();
		
	}
	
	private void deplacer1() {
		
		reinitialiser();
		
		try {
			
			Files.move(fileSystem.getPath(cheminFichier1), fileSystem.getPath(cheminFichier2), StandardCopyOption.REPLACE_EXISTING);
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		controler("Move");
		
	}
	
	private void deplacer2() {
		
		  reinitialiser();
		  
		  File fichierSource = null;
	      File fichierCible = null;
	    
	      
	      try {  
	      
	    	 fichierSource = new File(cheminFichier1);
	         fichierCible = new File(cheminFichier2);
	         fichierSource.renameTo(fichierCible);
	         
	      } catch(Exception e) {
	         e.printStackTrace();
	      }
	      
	      controler("RenameTo");
	      
	}
	
	private void deplacer3() {
		
		reinitialiser();
		
		try {
			
            String[] commande = {"mv", cheminFichier1, cheminFichier2 };
            Process p = Runtime.getRuntime().exec(commande);
            p.waitFor();
    
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e2) {
			e2.printStackTrace();
        }
		
		controler("LigneDeCommande");
		
	}
	
	private void controler(String typeCopie) {
		
		String resultat = "";
		File fichierSource = new File (cheminFichier1);
		File fichierCible= new File (cheminFichier2);
		
		if(!fichierSource.exists() && fichierCible.exists())
			resultat = "Succ�s de la copie : "+ typeCopie;
		else
			resultat = "Echec de la copie : "+ typeCopie;
		
		System.out.println(resultat);
				
	}
	
	private void reinitialiser() {
		
		try {
			
			File fichierSource = new File (cheminFichier1);
			
			if(!fichierSource.exists()) {
				FileWriter ecrivainFichier = new FileWriter(new File(cheminFichier1));
				BufferedWriter ecrivain = new BufferedWriter(ecrivainFichier);
				ecrivainFichier.close();
				ecrivain.close();
			}
			
			File fichierCible = new File (cheminFichier2);
			
			if(fichierCible.exists())
				fichierCible.delete();
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	
}
